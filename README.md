# Tech Stack Comparison 2024 #

Fitur Aplikasi :
- Transaksi payment
- Payment propagation to external services
- Audit Log

Requirement :
- Database Migration Tools
- Row Locking untuk generate running number
- Nested transaction untuk insert audit log
- Rollback transaction kalau call external service gagal

Contender :
- Java : Spring Boot Regular
- Java : Spring Boot Reactive
- PHP : Laravel
- Go : Fiber
- NodeJS : ExpressJS

Kriteria Perbandingan :
- Correctness pada waktu menghadapi:
  - Concurrent Request
  - External API Call error
- Developer Experience
- Lines of Code
- Throughput
- Latency
- Resource Consumption
